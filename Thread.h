#pragma once

#include "noncopyable.h"

#include <functional>
#include <thread>
#include <memory>
#include <unistd.h>
#include <string>
#include <atomic>

class Thread : noncopyable
{
public:
    using ThreadFunc = std::function<void()>;

    explicit Thread(ThreadFunc, const std::string &name = std::string());
    ~Thread();

    void start();
    void join();

    bool started() const { return started_; }
    pid_t tid() const { return tid_; }
    const std::string& name() const { return name_; }

    static int numCreated() { return numCreated_; }
private:
    void setDefaultName();

    bool started_;
    bool joined_;
    //指向线程的智能指针，不直接使用thread是因为thread一被创建就开始运行
    std::shared_ptr<std::thread> thread_;
    pid_t tid_;
    //EventLoopThread中的ThreadFunc，用于开启loop循环
    ThreadFunc func_;
    std::string name_;
    // 已创建线程个数
    static std::atomic_int numCreated_;
};